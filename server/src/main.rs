use tonic::{transport::Server};
use protos::echo::{EchoService};
use protos::echo::proto::echo_server::{EchoServer};

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    let addr = "[::1]:50051".parse().unwrap();
    let echo = EchoService::default();
    println!("Server listening on {}", addr);
    Server::builder()
        .add_service(EchoServer::new(echo))
        .serve(addr)
        .await?;
    Ok(())
}
