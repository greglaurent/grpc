pub mod proto {
    tonic::include_proto!("echo");
}

use tokio::sync::mpsc;
use tonic::{Streaming, Request, Response, Status};
use proto::echo_server::{Echo};
use proto::{HelloResponse, HelloRequest};

#[derive(Default)]
pub struct EchoService { }

#[tonic::async_trait]
impl Echo for EchoService {

    type SendChannelStream = mpsc::Receiver<Result<HelloResponse, Status>>;
    type BidirectionalStream = mpsc::Receiver<Result<HelloResponse, Status>>;

    async fn send(&self,request:Request<HelloRequest>)->Result<Response<HelloResponse>,Status> {
        Ok(Response::new(HelloResponse{
             message:format!("Hello {}",request.get_ref().name),
        }))
    }

    async fn send_channel(&self, request: Request<HelloRequest>) 
        -> Result<Response<Self::SendChannelStream>, Status> {
        let (mut tx, rx) = mpsc::channel(4);
        tokio::spawn(async move {
            for _ in 0..4 {
                tx.send(
                    Ok(HelloResponse {message: format! ("hello"),})
                ).await;
            }
        });

        Ok(Response::new(rx))
    }

    async fn receive_channel(&self, request: Request<Streaming<HelloRequest>>) 
        -> Result<Response<HelloResponse>, Status> {
        let mut stream = request.into_inner();
        let mut message = String::from("");

        while let Some(r) = stream.message().await? {
            message.push_str(&format!("Hello {}\n", r.name))
        }

        Ok(Response::new(HelloResponse { message }))
    }

    
    async fn bidirectional(&self, request: Request<Streaming<HelloRequest>>) 
        -> Result<Response<Self::BidirectionalStream>, Status> {
        let mut streamer = request.into_inner();
        let (mut tx, rx) = mpsc::channel(4);
        tokio::spawn(async move {
            while let Some(r) = streamer.message().await.unwrap() {
                tx.send(
                    Ok(HelloResponse { message: format!("hello {}", r.name),
                })).await;
            }
        });

        Ok(Response::new(rx))
    }
}
