use futures::stream::iter;
use protos::echo::proto::{HelloRequest};
use protos::echo::proto::echo_client::{EchoClient};

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    let channel = tonic::transport::Channel::from_static("http://[::1]:50051")
    .connect()
    .await?;
    let mut client = EchoClient::new(channel);

    //request
    let request = tonic::Request::new(
        HelloRequest {
           name:String::from("greg")
        },
    );

    let response = client.send(request).await?.into_inner();
    println!("RESPONSE={:?}", response);

    //request stream
    let stream_request = tonic::Request::new(
        HelloRequest {
           name:String::from("greg")
        },
    );

    let mut stream_response = client.send_channel(stream_request).await?.into_inner();
    while let Some(x) = stream_response.message().await? {
        println!("STREAM_RESPONSE={:?}", x);
    }

    //response stream
    let req = tonic::Request::new(
        iter(vec![
            HelloRequest {
                name: String::from("greg"),
            },
            HelloRequest {
                name: String::from("dave"),
            },
            HelloRequest {
                name: String::from("yawar"),
            },
        ]));

    let stream_req_response = client.receive_channel(req).await?.into_inner();
    println!("STREAM_REQ_RESPONSE={}", stream_req_response.message);


    //bidirectional stream
    let b_req = tonic::Request::new(
        iter(vec![
            HelloRequest {
                name: String::from("greg"),
            },
            HelloRequest {
                name: String::from("dave"),
            },
            HelloRequest {
                name: String::from("yawar"),
            },
        ]));

    let mut b_response = client.bidirectional(b_req).await?.into_inner();
    while let Some(r) = b_response.message().await? {
        println!("BIDIRECTIONAL = {:?}", r);
    }

    Ok(())
}
